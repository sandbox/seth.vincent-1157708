<?php
/**
 * @file
 * place.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function place_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "openlayers" && $api == "openlayers_maps") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function place_node_info() {
  $items = array(
    'place' => array(
      'name' => t('Place'),
      'base' => 'node_content',
      'description' => t('Places you want to put on a map.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

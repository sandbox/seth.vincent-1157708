the locator. a drupal 7 distribution.

you can build the locator install profile by doing the following:

cd path/to/drupal/profiles/locator
drush make --no-core --contrib-destination=. locator.make

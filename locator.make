api = 2
core = 7.x

; Contrib

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.x-dev"

projects[openlayers][subdir] = "contrib"
projects[openlayers][version] = "2.x-dev"

projects[geofield][subdir] = "contrib"
projects[geofield][version] = "1.x-dev"

projects[geolocation_proximity][subdir] = "contrib"
projects[gelocation_proximity][version] = "1.0"

projects[features][subdir] = "contrib"
projects[features][version] = "1.0-beta2"

projects[views][subdir] = "contrib"
projects[views][version] = "3.x-dev"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.x-dev"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.x-dev"

projects[rules][subdir] = "contrib"
projects[rules][version] = "2.x-dev"

projects[token][subdir] = "contrib"
projects[token][version] = "1.x-dev"

projects[date][subdir] = "contrib"                                              
projects[date][version] = "2.x-dev"

projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.x-dev"

projects[image_resize_filter][subdir] = "contrib"
projects[image_resize_filter][version] = "1.12"

projects[insert][subdir] = "contrib"
projects[insert][version] = "1.0"

projects[filefield_sources][subdir] = "contrib"
projects[filefield_sources][version] = "1.4"

projects[media][subdir] = "contrib"
projects[media][version] = "1.0-beta4"

projects[styles][subdir] = "contrib"
projects[styles][version] = "2.0-alpha5"

projects[plupload][subdir] = "contrib"
projects[plupload][version] = "1.0-beta2"

projects[media_youtube][subdir] = "contrib"
projects[media_youtube][version] = "1.0-alpha4"

projects[mediaelement][subdir] = "contrib"
projects[mediaelement][version] = "1.0"

projects[media_gallery][subdir] = "contrib"
projects[media_gallery][version] = "1.0-beta4"

projects[mobile_tools][subdir] = "contrib"
projects[mobile_tools][version] = "2.x-dev"

projects[feeds][subdir] = "contrib"
projects[feeds][version] = "2.0-alpha3"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0-beta2"

projects[pux][subdir] = "contrib"
projects[pux][version] = "1.0-alpha2"

projects[context][subdir] = "contrib"
projects[context][version] = "3.x-dev"

projects[boxes][subdir] = "contrib"
projects[boxes][version] = "1.0-beta3"

projects[diff][subdir] = "contrib"
projects[diff][version] = "2.0-beta2"

projects[relation][subdir] = "contrib"
projects[relation][version] = "1.0-alpha2"

projects[conditional_fields][subdir] = "contrib"
projects[conditional_fields][version] = "3.x-dev"

projects[workbench][subdir] = "contrib"
projects[workbench][version] = "1.0-beta5"

projects[workbench_moderation][subdir] = "contrib"
projects[workbench_moderation][version] = "1.0-beta5"

projects[smart_ip][subdir] = "contrib"
projects[smart_ip][version] = "1.x-dev"

projects[messaging][subdir] = "contrib"
projects[messaging][version] = "1.0-alpha1"

projects[notifications][subdir] = "contrib"
projects[notifications][version] = "1.0-alpha1"

projects[votingapi][subdir] = "contrib"
projects[votingapi][version] = "2.x-dev"

projects[vote_up_down][subdir] = "contrib"
projects[vote_up_down][version] = "1.x-dev"

projects[search_api][subdir] = "contrib"
projects[search_api][version] = "1.x-dev"

projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "1.x-dev"

projects[google_admanager][subdir] = "contrib"
projects[google_admanager][version] = "2.x-dev"

projects[flag][subdir] = "contrib"
projects[flag][version] = "2.x-dev"

projects[menu_token][subdir] = "contrib"
projects[menu_token][version] = "1.0-alpha1"

projects[menu_block][subdir] = "contrib"
projects[menu_block][version] = "2.x-dev"

projects[menutree][subdir] = "contrib"
projects[menutree][version] = "1.x-dev"

projects[menu_attributes][subdir] = "contrib"
projects[menu_attributes][version] = "1.x-dev"

projects[message][subdir] = "contrib"
projects[message][version] = "1.x-dev"

projects[addtoany][subdir] = "contrib"
projects[addtoany][version] = "3.1"

projects[linkit][subdir] = "contrib"
projects[linkit][version] = "1.x-dev"

projects[linkit_views][subdir] = "contrib"
projects[linkit_views][version] = "1.x-dev"

projects[extlink][subdir] = "contrib"
projects[extlink][version] = "1.12"

projects[smartcrop][subdir] = "contrib"
projects[smartcrop][version] = "1.x-dev"

projects[compact_forms][subdir] = "contrib"
projects[compact_forms][version] = "1.x-dev"

projects[job_scheduler][subdir] = "contrib"
projects[job_scheduler][version] = "2.0-alpha2"

projects[field_slideshow][subdir] = "contrib"
projects[field_slideshow][version] = "1.x-dev"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.x-dev"

projects[webform][subdir] = "contrib"
projects[webform][version = "3.9"

projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = "1.x-dev"

projects[globalredirect][subdir] = "contrib"
projects[globalredirect][version] = "1.x-dev"

projects[page_title][subdir] = "contrib"
projects[page_title][version] = "2.x-dev"

projects[transliteration][subdir] = "contrib"
projects[transliteration][version] = "3.x-dev"

; Development tools

projects[devel][subdir] = "contrib"
projects[devel][version] = "1.x-dev"

projects[devel_themer][subdir] = "contrib"
projects[devel_themer][version] = "1.x-dev"


; contrib drupal sandbox projects

projects[erno][type] = "theme"
projects[erno][download][type] = "git"
projects[erno][download][url] = "http://git.drupal.org/sandbox/seth.vincent/1113610"

projects[toolbar_styles][subdir] = "contrib"
projects[toolbar_styles][type] = "module"
projects[toolbar_styles][download][type] = "git"
projects[toolbar_styles][download][url] = "http://git.drupal.org/sandbox/seth.vincent/1118774"


; contrib from github

projects[geocode][subdir] = "contrib"
projects[geocode][type] = "module"
projects[geocode][download][type] = "git"
projects[geocode][download][url] = "https://github.com/phayes/geocode.git"

; Contrib themes

projects[tao][version] = "3.0-beta3"
projects[rubik][version] = "4.0-beta5"

projects[compact_forms][subdir] = "contrib"
projects[compact_forms][version] = "1.x-dev"

projects[job_scheduler][subdir] = "contrib"
projects[job_scheduler][version] = "2.0-alpha2"

; Libraries

libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "https://sourceforge.net/projects/tinymce/files/TinyMCE/3.3.9.2/tinymce_3_3_9_2.zip"
libraries[tinymce][directory_name] = "tinymce"

; libraries[geophp][download[type] = "get"
; libraries[geophp][download][url] = "https://github.com/downloads/phayes/geoPHP/geoPHP.tar.gz"
; libraries[geophp][directory_name] = "geoPHP"
